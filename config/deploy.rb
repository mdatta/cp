require 'bundler/capistrano'
set :bundle_dir,'/home/arthyant/cp/shared/bundle/ruby/1.9.1/gems/'
set :user, "arthyant"
set :domain,'www.manudatta.net'
set :applicationdir, "cp"
set :application, "cp"
#set :deploy_to,"../../www/#{applicationdir}"
default_run_options[:pty]=true
set :scm, :git
# Or: `accurev`, `bzr`, `cvs`, `darcs`, `git`, `mercurial`, `perforce`, `subversion` or `none`
set :repository, "git@bitbucket.org:mdatta/cp.git"
set :deploy_via,:remote_cache
set :local_repository,'arthyant@manudatta.net:/home/arthyant/cp.git'
set :branch,'master'
set :git_shallow_clone,1
set :scm_verbose,true
set :scm_user,"mdatta"

server "www.manudatta.net",:web,:app,:db,:primary => true
#role :web, "www.manudatta.net", :user => "arthyant"                      # Your HTTP server, Apache/etc
#role :app, "www.manudatta.net", :user => "arthyant"                      # This may be the same as your `Web` server
#role :db,  "www.manudatta.net", :primary => true # This is where Rails migrations will run
set :use_sudo,false
#set :deploy_via,:copy
ssh_options[:keys] = [File.join(ENV["HOME"], ".ssh", "id_rsa")]




# this will place you app outside your /public_html
# and protect your app file from exposure
set :deploy_to, "/home/#{user}/#{application}"

set :scm, :git
# set :deploy_via, :remote_cache  # if your server has direct access to the repository
set :deploy_via, :copy  # if you server does NOT have direct access to the repository (default)
set :git_shallow_clone, 1  # only copy the most recent, not the entire repository (default:1)

ssh_options[:paranoid] = false
set :use_sudo, false

set :keep_releases, 2  # only keep a current and one previous version to save space




task :update_public, :roles => [:app] do
  run "chmod 755 #{release_path}/public"
  run "chmod 755 #{release_path}/public/dispatch.*"
end
# remove the .git directory and .gitignore from the current release
task :remove_git_directories, :roles => [:app] do
  run "rm -rfd #{release_path}/.git"
  run "rm #{release_path}/.gitignore"
end
# this lets us keep the system_stopped files in project
task :copy_system_stopped_files, :roles => [:app] do
  run "cp -f #{release_path}/public/system_stopped/* #{shared_path}/system_stopped/"
end

after "deploy:update_code", :update_public
after "deploy:update_code", :copy_system_stopped_files
after "deploy:update_code", :remove_git_directories


# create a symlink from the current/public to ~/public_html
task :create_public_html, :roles => [:app] do
  run "ln -fs #{current_path}/public ~/public_html"
end
after "deploy:cold", :create_public_html

# create a directory in shared to hold files that will be servered
# when the system is stopped, and a config directory
task :create_shared_directories, :roles => [:app] do
  run "mkdir -p #{shared_path}/system_stopped"
  run "mkdir -p #{shared_path}/config"
end
after "deploy:setup", :create_shared_directories 


# remove the symlink for ~/public_html
task :remove_public_html, :roles => [:app] do
  run "rm ~/public_html"
end

# creates a symlink to ~/public_html to the shared/system_stopped directory
# places files you want served in this dir for when the system is stopped
task :create_public_html_to_stopped, :roles => [:app] do
  run "ln -fs #{shared_path}/system_stopped ~/public_html"
end



# we need to override the default start/stop/restart functions
namespace :deploy do
  desc "Restart the web server. Killing all FCGI processes." 
  task :restart, :roles => :app do
    # for most hosts, all you need to do is stop all FCGI processing running
    run "killall -q dispatch.fcgi" 
    # but some hosts can restart by touching the dispatch file
    #run "chmod 755 #{current_path}/public/dispatch.fcgi" 
    #run "touch #{current_path}/public/dispatch.fcgi" 
  end

  desc "Start the web server. Really nothing to do for shared hosting."
  task :start, :roles => :app do
    remove_public_html
    create_public_html
    deploy.restart
  end

  desc "Stop the web server and present maintenance page."
  task :stop, :roles => :app do
    remove_public_html
    create_public_html_to_stopped
  end
  
  namespace :web do
    desc "Make application web accessible again."
    task :enable, :roles => [:app] do
      deploy.start
    end
    
    desc "Present system maintenance page to users."
    task :disable, :roles => [:app] do
      deploy.stop
    end
    
  end
end
# if you want to clean up old releases on each deploy uncomment this:
# after "deploy:restart", "deploy:cleanup"

# if you're still using the script/reaper helper you will need
# these http://github.com/rails/irs_process_scripts

# If you are using Passenger mod_rails uncomment this:
# namespace :deploy do
#   task :start do ; end
#   task :stop do ; end
#   task :restart, :roles => :app, :except => { :no_release => true } do
#     run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
#   end
# end
