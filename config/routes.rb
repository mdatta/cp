Crackphysics::Application.routes.draw do

  #get "purchases/pay_per_month"

  #get "purchases/pay_per_month_subscribe"

  #get "purchases/pay_half_yearly"

  #get "purchases/pay_yearly"

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :purchases
  get 'purchases/pay' => 'purchases#pay' , :as => 'purchases'
  get 'purchases/afterpay' => 'purchases#afterpay' , :as => 'after_purchases'
  # exam(s) available
  get 'exams/index' => 'exam#index' , :as => 'examitems'
  # creating a new test
  get 'test_items/restart' => 'test_items#restart' , :as => 'restart_test_item'
  get 'test_items/delete' => 'test_items#delete' , :as => 'delete_test_item'
  get 'test_items/index' => 'test_items#index' , :as => 'testitems'
  get 'test_items/new' => 'test_items#new', :as => 'new_test_item'
  get 'test_items/get_testitem_details' => 'test_items#get_testitem_details', :as => 'get_testitme_details'
  get 'test_items/update' => 'test_items#update', :as => 'update_test_item'
  put 'test_items/update' => 'test_items#update', :as => 'update_test_item'
  get 'test_supervisor/loop' => 'test_supervisor#loop' , :as => 'testloop'
  get 'report/list_tests' => 'report#list_tests' , :as => 'showtests'
  get 'report/show_units_bar_chart' => 'report#show_units_bar_chart' , :as => 'showunitsbarchart'
  get 'report/show_tests_line_chart' => 'report#show_tests_line_chart' , :as => 'showtestslinechart'
  get 'report/print_test' => 'report#print_test' , :as => 'printtestpdf'
  get 'question_statements/showf' => 'question_statements#showf' , :as => 'getquestion'
  get 'test_factory/create_unit_test' => 'test_factory#create_unit_test', :as => 'createunittest' 
  get 'unit/index' => 'unit#index' , :as => 'unititems'
  get 'unit/indexdd' => 'unit#indexdd', :as => 'unitslist' 
  get 'unit/indexnottaken' => 'unit#indexnottaken', :as => 'unitsnottakenlist' 
  get 'unit/show/:id' => 'unit#show' , :as => 'unit'
  get 'user/welcome' => 'user#welcome', :as => 'welcome' 
  get 'main/start' => 'main#start', :as => 'home' 
  get 'how_to_upgrade' => 'main#how_to_upgrade', :as => 'upgrade' 
  post 'user/login' => 'user#login', :as => 'userlogin' 
  get 'user/login' => 'user#login', :as => 'userlogin' 
  get 'user/show' => 'user#show', :as => 'usershow' 
  post 'user/signup' => 'user#signup', :as => 'usersignup' 
  post 'user/forgot_password' => 'user#forgot_password', :as => 'userforgot_password' 
  get 'user/forgot_password' => 'user#forgot_password', :as => 'userforgot_password' 
  put 'user/change_password' => 'user#change_password', :as => 'userchange_password' 
  get 'user/change_password' => 'user#change_password', :as => 'userchange_password' 
  get 'user/logout' => 'user#logout', :as => 'userlogout' 
  get 'user/signup' => 'user#signup', :as => 'usersignup'
  # test_types routes
  get 'exam/get_test_types/:exam_id/:format' => 'exam#get_test_types'
  get 'exam/get_units/:exam_id/:format' => 'exam#get_units'
  get 'help' => 'main#show_help' , :as => 'help' 
  get 'feedback' => 'feedbacks#new', :as => 'feedback'
  post 'feedback/create' => 'feedbacks#create', :as => 'feedbackcreate'
  get 'subscription/index' => 'subscription#index', :as => 'subscriptionindex' 
  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  root :to => "main#start"

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id(.:format)))'
  mount PaypalIpn, :at => '/paypal_ipn' 
end
