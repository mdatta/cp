env_dir=Rails.root+'config/environments'
require "#{env_dir}/localization_environment"
require 'localization'
require 'user_system'
require "#{env_dir}/user_environment"
Localization::load_localized_strings

