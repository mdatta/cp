$erbout = nil
$erbout_stack = []
class ERB
  def set_eoutvar_with_global_output(compiler, eoutvar = '_erbout')
    set_eoutvar_without_global_output(compiler, eoutvar)

    cmd = []
    cmd.push "$erbout_stack.push $erbout"
    cmd.push "#{eoutvar} = ''"
    cmd.push "$erbout = #{eoutvar}"
    compiler.pre_cmd = cmd

    cmd = []
    cmd.push "$erbout = $erbout_stack.pop"
    cmd.push eoutvar
    compiler.post_cmd = cmd
  end
  
  alias_method :set_eoutvar_without_global_output, :set_eoutvar
  alias_method :set_eoutvar, :set_eoutvar_with_global_output
end
