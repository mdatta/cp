class QuestionStatement < ActiveRecord::Base
  serialize :choices
  belongs_to :test_question
end
