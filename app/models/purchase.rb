class Purchase < ActiveRecord::Base
  belongs_to :subscription
  has_one :package
end
