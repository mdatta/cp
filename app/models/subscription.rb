class Subscription < ActiveRecord::Base
 belongs_to :user
 has_one :package
 has_one :exam
 def index
 end
 def is_active?
  now = Time.now()
  if self.purchase then
    return self.package.duration - (now + self.purchase.paid_at) > 0
  end
  return false
 end
end
