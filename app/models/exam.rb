class Exam < ActiveRecord::Base
  has_many :subscriptions
  serialize :test_type_ids
  serialize :unit_ids
end
