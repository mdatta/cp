class Subunit < ActiveRecord::Base
	belongs_to :unit
	has_many :concept_statements
end
