class TestItem < ActiveRecord::Base
  has_many :test_questions
  def strike_ratio 
     read_attribute(:correct_count).to_f / read_attribute(:question_count)
  end
  def corr_vs_incorr
    type = TestType.find_by_id( read_attribute(:test_type_id) )
    "Corr/Total=" + read_attribute(:correct_count).to_s + "/" + read_attribute(:question_count).to_s
  end 
  def test_xstr
    taken_on = read_attribute(:created_on)
    taken_on.to_s(:short) 
  end
  def test_lbl
    "Test"
  end 
  def google_chart_data_str
   created_on = read_attribute(:created_on)
   datestr = created_on.strftime("%Y,")+(created_on.mon-1).to_s+created_on.strftime(",%d,%H,%M")
   return "[new Date(" + datestr + ")," +strike_ratio.to_s + ", null]"
  end
end
