class TestQuestion < ActiveRecord::Base
	serialize :answer_sequence
  belongs_to :test_item
  belongs_to :question_statement
  has_one :question_statement
  def is_correct
    return current_choice == correct_choice
  end
  def mapindex(index)
    retval = answer_sequence.find_index(index) + 1
    return retval
  end

end
