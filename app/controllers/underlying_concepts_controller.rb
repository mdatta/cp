class UnderlyingConceptsController < ApplicationController
  def index
    list
    render :action => 'list'
  end

  # GETs should be safe (see http://www.w3.org/2001/tag/doc/whenToUseGet.html)
  verify :method => :post, :only => [ :destroy, :create, :update ],
         :redirect_to => { :action => :list }

  def list
    @underlying_concept_pages, @underlying_concepts = paginate :underlying_concepts, :per_page => 10
  end

  def show
    @underlying_concept = UnderlyingConcept.find(params[:id])
  end

  def new
    @underlying_concept = UnderlyingConcept.new
  end

  def create
    @underlying_concept = UnderlyingConcept.new(params[:underlying_concept])
    if @underlying_concept.save
      flash[:notice] = 'UnderlyingConcept was successfully created.'
      redirect_to :action => 'list'
    else
      render :action => 'new'
    end
  end

  def edit
    @underlying_concept = UnderlyingConcept.find(params[:id])
  end

  def update
    @underlying_concept = UnderlyingConcept.find(params[:id])
    if @underlying_concept.update_attributes(params[:underlying_concept])
      flash[:notice] = 'UnderlyingConcept was successfully updated.'
      redirect_to :action => 'show', :id => @underlying_concept
    else
      render :action => 'edit'
    end
  end

  def destroy
    UnderlyingConcept.find(params[:id]).destroy
    redirect_to :action => 'list'
  end
end
