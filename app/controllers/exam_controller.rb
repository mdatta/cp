class ExamController < ApplicationController
  def index
    #subscriptions = Subscriptions.joins(:user)
    @Exams = Exam.all
  end  
  def get_test_types
    test_type_ids = Exam.find(params[:exam_id]).test_type_ids
    @test_types = TestType.where(id: test_type_ids)
    render :layout => false and return if params[:format] == 'nolayout'
  end
  def get_units
    unit_ids = Exam.find(params[:exam_id]).unit_ids
    logger.debug "Unit ids are:",unit_ids
    @units = Unit.where(id: unit_ids)
    render :layout => false and return if params[:format] == 'nolayout'
  end
end
