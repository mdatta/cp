class PurchasesController < ApplicationController
  def pay
    @amount = 3
    month = params[:month].to_i
    user_id = session[:user].id
    pay = Purchase.new(:user_id => user_id,:month_count =>month)
    pay.save 
    call_paypal  
  end
  def afterpay
  end
  def call_paypal
    pay_request = PaypalAdaptive::Request.new
    ipn_url="https://localhost:3000/"
    data = {
    :returnUrl => "http://localhost:3000"+after_purchases_path, 
    :requestEnvelope => {:errorLanguage => "en_US"},
    :currencyCode => "USD",
    :receiverList =>
      { :receiver => [
        { :email => "iconok_1355821337_biz@gmail.com", :amount=> @amount}
      ]},
      :cancelUrl => "http://localhost:3000"+home_path,
      :actionType => "PAY",
      :ipnNotificationUrl => ipn_url
    }
    logger.debug data.inspect 
    pay_response = pay_request.pay(data)
    logger.debug pay_response.inspect
  if pay_response.success?
      logger.debug pay_response.inspect
      redirect_to pay_response.approve_paypal_payment_url
  else
      puts pay_response.errors.first['message']
      redirect_to home_path, :notice => "Something went wrong. Please contact support."
   end
  end
end
