class QuestionStatementsController < ApplicationController
  def index
    list
    render :action => 'list'
  end

  # GETs should be safe (see http://www.w3.org/2001/tag/doc/whenToUseGet.html)
  verify :method => :post, :only => [ :destroy, :create, :update ],
         :redirect_to => { :action => :list }

  def list
    @question_statement_pages, @question_statements = paginate :question_statements, :per_page => 10
  end

  def show
    @question_statement = QuestionStatement.find(params[:id])
  end
  
  def showf
    @qtn = QuestionStatement.find(params[:id])
    @choices = @qtn.choices
    render :partial => 'showf', :layout => false 
  end

  def new
    @question_statement = QuestionStatement.new
  end

  def create
    @question_statement = QuestionStatement.new(params[:question_statement])
    if @question_statement.save
      flash[:notice] = 'QuestionStatement was successfully created.'
      redirect_to :action => 'list'
    else
      render :action => 'new'
    end
  end

  def edit
    @question_statement = QuestionStatement.find(params[:id])
  end

  def update
    @question_statement = QuestionStatement.find(params[:id])
    if @question_statement.update_attributes(params[:question_statement])
      flash[:notice] = 'QuestionStatement was successfully updated.'
      redirect_to :action => 'show', :id => @question_statement
    else
      render :action => 'edit'
    end
  end

  def destroy
    QuestionStatement.find(params[:id]).destroy
    redirect_to :action => 'list'
  end
end
