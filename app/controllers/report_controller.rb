class ReportController < ApplicationController
  before_filter :login_required
  def list_tests
	  user_id = session['user'].id
	  @tests = TestItem.find(:all,:conditions => {:user_id => "#{user_id}",:status => "submit"},:order=> 'created_on' )
  end
  def show_tests_line_chart
	  user_id = session['user'].id
    tests_taken = TestItem.find_all_by_user_id(user_id,:order => :created_on)
    if tests_taken.length > 0 
      @linechartdata =[["Test no/Taken on","Correct/Total"]]
      count = 1
      tests_taken.each { |t|
        test_questions = TestQuestion.find_all_by_test_item_id(t.id)
        corrects = 0 
        test_questions.each{ |tq| corrects = corrects + 1 if  tq.current_choice == tq.correct_choice }
        total = test_questions.size
        correctratio = corrects.to_f / total ;
        @linechartdata << [ t.created_on.strftime("Test no #{count} on (%d/%m/%y) ")+total.to_s , correctratio ]
        count = count + 1
      }
    else 
      @linechartdata = nil
    end

  end 
  def show_units_bar_chart 
	  user_id = session['user'].id
    tests_taken = TestItem.find_all_by_user_id(user_id).select{|t| t.status == 'submit'}.collect { |t| t.id }
    logger.debug ">>>>>>>>>>>>>Tests taken",tests_taken
    if tests_taken.length > 0 
      test_questions = TestQuestion.find_all_by_test_item_id(tests_taken)
      is_correct = {}
      which_qsid = {}
      which_uid = {}
      @units = []
      test_questions.each{ |t| is_correct[t.id] = t.current_choice == t.correct_choice
      which_qsid[t.id] = t.question_statement_id }
      test_question_ids = test_questions.collect { |tq| tq.question_statement_id }.sort
      UnderlyingConcept.find_all_by_question_statement_id(test_question_ids).each{ |x| which_uid[x.question_statement_id]=x.unit_id
      @units<<Unit.find(x.unit_id)
      }
      #for each unit id find how many correct
      @unit_results = {}
      @units.each{ |u|
        one_result = {}
        one_result[:name] = u.name
        one_result[:corrects] = 0 
        one_result[:total] = 0 
        @unit_results[u.id] = one_result
      }
      test_questions.each{ |q|
        logger.debug @unit_results
        logger.debug q.inspect 
        uid = which_uid[ which_qsid[q.id] ]
        logger.debug "Uid is #{uid}" 
        result = @unit_results[uid]
        if result != nil
          result[:total] = result[:total] + 1
          if is_correct[q.id] then
            result[:corrects] = result[:corrects] + 1
          end
        end
      }
    @barchartdata =[["Unit","Correct","Wrong"]]
    @unit_results.each { |k,v| @barchartdata << [v[:name]+"-"+v[:total].to_s,v[:corrects],v[:total]-v[:corrects]] }
    else
      @units = nil 
   end
  end
  def print_test
    test_id = params[:test_id]
    @test = TestItem.find(test_id)
    @test_no = params[:test_no].to_i + 1
    @test_date = (@test.created_on).strftime("%d/%m/%y")
    @user = session['user']
    @q = TestQuestion.find_all_by_test_item_id(test_id)
    @qs = @q.collect{ |q| QuestionStatement.find_by_id(q.question_statement_id) }
  end
  def pdf
    @article = Test.find(@params["test_id"])
    #header
    @header = getheader(@params[:test_no],@params[:test_id])
    logger.info("header is "+@header.to_s)
    #rest of questions
    @questions = Test.find_all(@params[:test_id])
    generator = IO.popen("htmldoc -t pdf --path \".;http://#{@request.env["HTTP_HOST"]}\" --webpage -", "w+")
    generator.puts render("report/pdf")
    generator.close_write
    send_data(generator.read, :filename => "TestNumber#{@header['testno']}.pdf", :type => "application/pdf")
  end
  def pdf_old
	_pdf = PDF::Writer.new
	_pdf.select_font "Times-Roman"
	header = get_header(@params[:test_no],@params[:test_id])
	fs = _pdf.font_size
	_pdf.font_size = fs + 5
	_pdf.text header['name']
	_pdf.font_size = fs + 3
	_pdf.text("Test Number : "+header['testno'])
	_pdf.text("Taken on : "+header['startdate'])
	y0 = _pdf.y - 10
	x0 = 10
	_pdf.line(_pdf.absolute_left_margin,y0,_pdf.absolute_right_margin,y0).stroke
	_pdf.font_size = fs
	_pdf.y = y0 - 4
	questions = []
	answers = []
	correct = []
	choice = []
	testquestions = TestQuestion.find(:all, :conditions => ["test_id = #{@params[:test_id]}"])
	for testquestion in testquestions do
		q_stmt_id = testquestion.question_statement_id
		q_stmt = QuestionStatement.find(q_stmt_id)
		questions << q_stmt.statement
		choice << testquestion.choice
		correct << (testquestion.choice==4)
		seq = testquestion.answer_sequence
		a_stmts = [q_stmt.correct_ans_stmt, q_stmt.ans_stmt_1,q_stmt.ans_stmt_2,q_stmt.ans_stmt_3]
		answers << a_stmts
	end
	maintext = ""
	i = 0 
	for aq in questions do
		map_html(_pdf,"Q#{i+1}: "+aq)
		oldmargin = _pdf.left_margin
		_pdf.left_margin = oldmargin + 10
		if choice[i] == 1 and correct[i] 
			_pdf.fill_color  Color::RGB::Blue
			map_html(_pdf,"a) "+answers[i][0])
			_pdf.fill_color Color::RGB::Black
		elsif choice[i] == 1
			_pdf.fill_color  Color::RGB::Red
			map_html(_pdf,"a) "+answers[i][0])
			_pdf.fill_color Color::RGB::Black
		else
			map_html(_pdf,"a) "+answers[i][0])
		end
		if choice[i] == 2 and correct[i] 
			_pdf.fill_color  Color::RGB::Blue
			map_html(_pdf,"b) "+answers[i][1])
			_pdf.fill_color Color::RGB::Black
		elsif choice[i] == 2
			_pdf.fill_color  Color::RGB::Red
			map_html(_pdf,"b) "+answers[i][1])
			_pdf.fill_color Color::RGB::Black
		else
			map_html(_pdf,"b) "+answers[i][1])
		end
		if choice[i] == 3 and correct[i] 
			_pdf.fill_color  Color::RGB::Blue
			map_html(_pdf,"c) "+answers[i][2])
			_pdf.fill_color Color::RGB::Black
		elsif choice[i] == 3
			_pdf.fill_color  Color::RGB::Red
			map_html(_pdf,"c) "+answers[i][2])
			_pdf.fill_color Color::RGB::Black
		else
			map_html(_pdf,"c) "+answers[i][2])
		end
		if choice[i] == 4 and correct[i] 
			_pdf.fill_color  Color::RGB::Blue
			map_html(_pdf,"d) "+answers[i][3])
			_pdf.fill_color Color::RGB::Black
		elsif choice[i] == 4
			_pdf.fill_color  Color::RGB::Red
			map_html(_pdf,"d) "+answers[i][3])
			_pdf.fill_color Color::RGB::Black
		else
			map_html(_pdf,"d) "+answers[i][3])
		end
		_pdf.left_margin = oldmargin
		i = i+1
	end

	
	# from the test id and the user id get all the questions and their answers and if 
	# wrong print it in red
	send_data _pdf.render, :filename => "test#{@params[:test_no]}.pdf",
			    :type => "application/pdf"
  end
  def getheader(test_no,test_id)
  	 user = session['user']
	 header = {}
	 header['name'] = "#{user.lastname.swapcase}, #{user.firstname.swapcase}"
	 header['testno'] = test_no.to_s
	 test = Test.find(test_id)
	 header['date'] = (test.created_on).strftime("%d/%m/%y")
	 return header
  end
  def map_html(pdf,str)
	  indexf = str.index("<sup>")
	  unless indexf.nil?
		  indexb = str.rindex("</sup>")
		  pdf.text str[0..(indexf-1)]
		  width = pdf.text_width( str[0..(indexf-1)] ) + pdf.left_margin
		  suptxt = str[(indexf+5)..(indexb-1)]
		  resttext = str[(indexb+6)..str.length]
		  y0 = pdf.y
		  fs = pdf.font_size
		  pdf.font_size = fs - 3
		  pdf.y = pdf.y+5
		  pdf.add_text(width,pdf.y,suptxt)
		  width = width + pdf.text_width(suptxt)
		  pdf.font_size = fs
		  pdf.y = y0
		  pdf.add_text(width,pdf.y,resttext)
	  else
		  pdf.text str
	  end
  end
end
