class ApplicationController < ActionController::Base
  include Localization
  include UserSystem
  require_dependency 'user'
  helper :all
  protect_from_forgery
end
