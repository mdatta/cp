class SubunitsController < ApplicationController
  def index
    list
    render :action => 'list'
  end

  # GETs should be safe (see http://www.w3.org/2001/tag/doc/whenToUseGet.html)
  verify :method => :post, :only => [ :destroy, :create, :update ],
         :redirect_to => { :action => :list }

  def list
    @subunit_pages, @subunits = paginate :subunits, :per_page => 10
  end

  def show
    @subunit = Subunit.find(params[:id])
  end

  def new
    @subunit = Subunit.new
  end

  def create
    @subunit = Subunit.new(params[:subunit])
    if @subunit.save
      flash[:notice] = 'Subunit was successfully created.'
      redirect_to :action => 'list'
    else
      render :action => 'new'
    end
  end

  def edit
    @subunit = Subunit.find(params[:id])
  end

  def update
    @subunit = Subunit.find(params[:id])
    if @subunit.update_attributes(params[:subunit])
      flash[:notice] = 'Subunit was successfully updated.'
      redirect_to :action => 'show', :id => @subunit
    else
      render :action => 'edit'
    end
  end

  def destroy
    Subunit.find(params[:id]).destroy
    redirect_to :action => 'list'
  end
end
