class ConceptStatementsController < ApplicationController
  def index
    list
    render :action => 'list'
  end

  # GETs should be safe (see http://www.w3.org/2001/tag/doc/whenToUseGet.html)
  verify :method => :post, :only => [ :destroy, :create, :update ],
         :redirect_to => { :action => :list }

  def list
    @concept_statement_pages, @concept_statements = paginate :concept_statements, :per_page => 10
  end

  def show
    @concept_statement = ConceptStatement.find(params[:id])
  end

  def new
    @concept_statement = ConceptStatement.new
  end

  def create
    @concept_statement = ConceptStatement.new(params[:concept_statement])
    if @concept_statement.save
      flash[:notice] = 'ConceptStatement was successfully created.'
      redirect_to :action => 'list'
    else
      render :action => 'new'
    end
  end

  def edit
    @concept_statement = ConceptStatement.find(params[:id])
  end

  def update
    @concept_statement = ConceptStatement.find(params[:id])
    if @concept_statement.update_attributes(params[:concept_statement])
      flash[:notice] = 'ConceptStatement was successfully updated.'
      redirect_to :action => 'show', :id => @concept_statement
    else
      render :action => 'edit'
    end
  end

  def destroy
    ConceptStatement.find(params[:id]).destroy
    redirect_to :action => 'list'
  end
end
