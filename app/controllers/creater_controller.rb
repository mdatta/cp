class CreaterController < ApplicationController
  before_filter :login_required
  def show_tests
	 user_id = session['user'].id
   user_sub_level = UserProfile.find_by_user_id(user_id).subscription_level
   @AvailableTests = (UserEntitlement.find_all_by_subscription_level(user_sub_level).map{ |ent| TestType.find_all_by_id(ent.test_type_id)}).flatten.map{ |t| t.name }
   @AllTests = (TestType.find(:all)).map{ |t| t.name }
  end
  # if user chooses unit test show him units available
  def list_units
        @categories = Category.find_all
  end
  def find_units_by_user
    units = []
    subcategories = []
    user_id = session['user'].id
    tests = Test.find_all_by_user_id(user_id)
    for test in tests do
      test_id = test.id
      test_questions = TestQuestion.find_all_by_test_id(test_id)
      for test_question in test_questions do
        question_statements = QuestionStatement.find_all(test_question.id)
        for question_statement in question_statements do
          underlying_concepts = UnderlyingConcept.find_all_by_question_statement_id(question_statement.id)
          for underlying_concept in underlying_concepts do
            concept_statements = ConceptStatement.find_all(underlying_concept.concept_statement_id)
            for concept_statement in concept_statements do
              subcategories << Subcategory.find_all(concept_statement.subcategory_id)
            end
          end
        end
      end
    end
    return subcategories
  end
  def get_test_info
    session['questionscount'] = params[:questioncount] 
    case  params[:test][:name]
      when "unit" 
        @categories = Category.find(:all)
        render :action => 'list_units'
      when "full"
        redirect_to :controller => "test_factory", :action => "create_full_test"
      when "progressive"
        redirect_to :controller => "creater", :action => "get_next_test"
      else
        logger.info("error")
    end
  end
  def get_next_test
    user_profile = UserProfile.find_by_user_id(session['user'].id)
    @units_attempted = user_profile.units_attempted
    @categories = Category.find_all
    @total_units = (1..@categories.length).to_a
    @units_left = @total_units - @units_attempted
  end
end
