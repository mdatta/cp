class UnitController < ApplicationController
  before_filter :login_required
  def index
    @units = Unit.find(:all)
    render :layout => false
  end
  # render drop down for user to pick
  def indexdd
    @units = Unit.find_all_by_show TRUE
    render :layout => false
  end
  def indexnottaken
    taken = User.find_by_id(session[:user].id).units_attempted
    logger.debug "Taken-->#{taken}"
    units = Unit.find(:all)
    logger.debug "Taken[0] = #{taken[0]} #{taken[1].class}"
    logger.debug "Rindex = #{taken.rindex(20)}"
    @units = units.find_all { |unit| taken.rindex(unit.id) == nil } 
    render :layout => false
  end
  def show
    id = params[:id].to_i
    @qids = UnderlyingConcept.find_all_by_unit_id(id).collect { |x| x.question_statement_id }
  end
end
