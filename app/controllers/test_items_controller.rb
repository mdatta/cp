class TestItemsController < ApplicationController
  before_filter :login_required
  before_filter :can_show_list?#, :only => [:new,:delete,:update]
  def index
   @AllTests = (TestType.find(:all)).map{ |t| t.name }
   user_id = session[:user].id
   @unfinished = TestItem.where(:status=>'new',:user_id=>user_id)
   @user = User.find_by_id(user_id)
   sleep(5)
   @totaltests = TestItem.where(:user_id => user_id , :status => 'submit').length
   if  @totaltests >= 2 then
    @unfinished = [] 
   end
  end
  def restart
    testitem_id = params[:id].to_i
    session[:test_id] = testitem_id
    @q =  TestQuestion.where(:test_item_id=>testitem_id).first
    session[:curr_qid] = @q.id
    redirect_to :action => "update", :page => "1",:layout=>true
  end
  def delete
    testitem_id = params[:id].to_i
    testitem = TestItem.where(:id => testitem_id ).first
    testitem.status = 'delete'
    testitem.save 
    redirect_to :action => 'get_testitem_details'
  end 
  def get_testitem_details
   user_id = session[:user].id
   exam_ids = Subscription.where(:user_id=>user_id).collect { |s| s.exam_id }
   @exams = Exam.where(:id=>exam_ids) 
   @unfinished = TestItem.where(:status=>'new',:user_id=>user_id)
  end
  def new
    required_params = ["exam_id","test_item_id","numofqs"]
    redirect_to :action => :get_testitem_details and return unless (required_params - params.keys).empty? 
    testitem = params[:test_item_id]
    num_of_qs = params[:numofqs].to_i
    testitem = TestType.find(testitem)
    logger.debug testitem.code
    case testitem.code
    when 'unit'
      unit = params[:unit]
      create_unit_test(unit,num_of_qs)
      logger.debug "===> Created unit test <====" 
    when 'full'
      create_full_test(num_of_qs)
      logger.debug "===> Created full test <====" 
    when 'progressivetest'
      unit = params[:unit]
      create_progressive_test(num_of_qs,unit)
      logger.debug "===> Created progressive test <====" 
    end 
    logger.debug "===> Finished creating test <====== #{testitem=='progressive'}" 
    redirect_to :action => "update", :page => "1",:layout=>true
  end
  
  def update
    logger.debug session.inspect
    currtest = TestItem.find_by_id(session[:test_id])
    q = TestQuestion.find_by_id(session[:curr_qid])
    q.current_choice = params[:answers_buttons].to_i if params[:answers_buttons]
    q.current_choice = params[:currchoice].to_i if params[:currchoice]
    q.save
    if params[:save] # if test is submitted
      currtest.status = 'submit'
      questions = TestQuestion.find_all_by_test_item_id(session[:test_id])
      currtest.correct_count = (questions.find_all{ |q| q.current_choice == q.correct_choice}).length 
      currtest.save
      testtypeid = currtest.test_type_id
      testname = TestType.find_by_id(testtypeid).code
      logger.debug "The test name is **********#{testname}****************"
      case testname 
      when 'unit', 'progressive'
        #user_id = session[:user].id
        #u = User.find_by_id(user_id)
        #unitssoffar = u.units_attempted
        #logger.debug "New unitid"
        #unitssoffar << session[:curr_unit]
        #logger.debug unitssoffar
        #u.units_attempted = unitssoffar.uniq
        #u.save
      end 
      session[:test_id] = nil
      redirect_to home_path
    elsif params[:cancel] # if cancel delete it
      currtest.delete
      session[:test_id] = nil
      redirect_to home_path
    else # else call update again
      questions = TestQuestion.find_all_by_test_item_id(session[:test_id],:order => 'id')
      logger.debug questions.inspect
      i = params[:page].to_i - 1
      @q = questions[i] 
      @qs = QuestionStatement.where( :id => @q.question_statement_id  ).first 
      #@question = currtest.get_question_at params[:page]
      @qrange = Array.new( questions.length ){|i| i+1}
      @unanscount = ( questions.find_all { |q| q.current_choice == 0 } ).length
      session[:curr_qid] = @q.id
      render(:layout => false) unless params[:layout] 
    end
  end
   
  def create_n_questions(qstmt_ids,testid)
   qstmt_ids.each { |id|
     qs = QuestionStatement.find_by_id(id)
     logger.debug "===>Choices are #{qs.choices}<==="
     shuffledchoices = shuffle( Array.new(qs.choices.length){|i| i+1} )
     logger.debug "===>Shuffled choices are #{shuffledchoices}<==="
     q = TestQuestion.create({:test_item_id=>testid,:question_statement_id=>id,:current_choice=>0,:answer_sequence=>shuffledchoices})
     correct_choice = q.mapindex( qs.correct_choice_index )
     q.correct_choice = correct_choice
     q.save
   }
  end
# create unit test
  def create_unit_test(unit,num_of_qs)
    user_id = session[:user].id
    qstmt_ids = get_n_questions_from_catid(unit,num_of_qs)
    testtypeid = TestType.where(:code=>'unit').first.id
    TestItem.transaction do 
      testitem = TestItem.create({:user_id=>user_id,:test_type_id=>testtypeid,:question_count=>num_of_qs})
      create_n_questions(qstmt_ids,testitem.id)
      session[:test_id] = testitem.id
      @q =  TestQuestion.where(:test_item_id=>testitem.id).first
      session[:curr_qid] = @q.id
      session[:curr_unit] = unit.to_i
    end
  end
 def can_show_list?
   retval = true 
   user_id = session[:user].id
   @user = User.find_by_id(user_id)
   @totaltests = TestItem.where(:user_id => user_id , :status => 'submit').length
   if  @totaltests >= 2 then
     retval = false
   end
   return retval
 end 
# create full test
  def create_full_test(total_questions)
   qstmts = QuestionStatement.find(:all,:limit => total_questions,:order=>get_order_by)
   user_id = session[:user].id
   qstmts = qstmts.uniq
   qstmt_ids = qstmts.collect { |c| c.id } 
   testtypeid = TestType.where(:code=>'full').first.id
   TestItem.transaction do 
      testitem = TestItem.create({:user_id=>user_id,:test_type_id=>testtypeid,:question_count=>total_questions})
      create_n_questions(qstmt_ids,testitem.id)
      session[:test_id] = testitem.id
      @q =  TestQuestion.where(:test_item_id=>testitem.id).first
      session[:curr_qid] = @q.id
    end
  end
  def get_order_by
   orderby = "RANDOM()"
   ismysql = ActiveRecord::Base.connection.instance_values["config"][:adapter].index("mysql")
   orderby = "RAND()" if ismysql
   return orderby
  end
# create progressive test
  def create_progressive_test(total_questions,unit)
   logger.debug "==> Making progressive test..New unit is #{unit}...Total Questions are #{total_questions}"
   total_questions = total_questions.to_i
   unit = unit.to_i
   units = (User.find_by_id(session[:user].id).units_attempted << unit).find_all { |u| u != nil }
   qstmt_ids = []
   qsperunit = total_questions / units.length
   qsperunit = 1 if qsperunit == 0 
   logger.debug "Units attempted = #{units.inspect} Questions per unit are #{qsperunit}"
   units.each { |unit_id|
    qstmt_ids = qstmt_ids + get_n_questions_from_catid(qsperunit,unit_id)
   }
   # fill rest from current test
   qstmt_ids = qstmt_ids +  get_n_questions_from_catid(total_questions - qstmt_ids.length , unit )
   logger.debug "Question statements ids-> #{qstmt_ids.inspect}"
   user_id = session[:user].id
   testtypeid = TestType.where(:code=>'progressive').first.id
   TestItem.transaction do 
      testitem = TestItem.create({:user_id=>user_id,:test_type_id=>testtypeid,:question_count=>total_questions})
      create_n_questions(qstmt_ids,testitem.id)
      session[:test_id] = testitem.id
      @q =  TestQuestion.where(:test_item_id=>testitem.id).first
      session[:curr_qid] = @q.id
      session[:curr_unit] = unit
    end
    end
  end
##########################################
#helper functions
  def get_n_questions(total_questions)
    qstmt_ids = []
    concepts=UnderlyingConcept.find(:all,:select=>"question_statement_id",:limit=>total_questions,:order=>get_order_by)
    logger.info concepts
    qstmt_ids = concepts.collect { |c| c.question_statement_id } 
    return qstmt_ids.uniq
  end
  def get_n_questions_from_catid(cat_id,total_questions)
    qstmt_ids = []
    user_id = session[:user].id
    exam_id = Subscription.find_by_user_id(user_id,:select=>"exam_id").exam_id
    concepts=UnderlyingConcept.find_all_by_unit_id_and_exam_id(cat_id ,exam_id ,:select=>"question_statement_id",:limit=>total_questions,:order=>get_order_by)
    logger.info concepts
    qstmt_ids = concepts.collect { |c| c.question_statement_id } 
    return qstmt_ids.uniq
  end
  def shuffle(arr)
   logger.debug "===>Arr #{arr}<==="
   logger.debug "===Shuffled arr #{arr.sort{rand-0.5}}<==="
   return  arr.sort { rand-0.5 }
  end 
###########################################
