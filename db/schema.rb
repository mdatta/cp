# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20140501090628) do

  create_table "analytics", :force => true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "categories", :force => true do |t|
    t.text "name"
  end

  create_table "concept_statements", :force => true do |t|
    t.integer "subunit_id"
    t.text    "text"
    t.text    "formula"
  end

  add_index "concept_statements", ["subunit_id"], :name => "subunit_id"

  create_table "exams", :force => true do |t|
    t.string "exam_name",     :limit => 25
    t.text   "test_type_ids"
    t.text   "unit_ids"
  end

  create_table "exams_has_units", :id => false, :force => true do |t|
    t.integer "exams_id", :null => false
    t.integer "units",    :null => false
  end

  add_index "exams_has_units", ["units"], :name => "fk_exams_has_units"

  create_table "feedbacks", :force => true do |t|
    t.text   "feedback",               :null => false
    t.string "emailid",  :limit => 20, :null => false
  end

  create_table "purchases", :force => true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
    t.integer  "month_count"
    t.date     "paid_at"
    t.string   "state",       :limit => 10
  end

  create_table "question_statements", :force => true do |t|
    t.text    "statement"
    t.text    "choices"
    t.string  "qtype",                :limit => 10
    t.integer "correct_choice_index"
    t.string  "image_loc",            :limit => 45
    t.integer "level"
  end

  create_table "subscriptions", :force => true do |t|
    t.integer "user_id",    :null => false
    t.integer "exam_id",    :null => false
    t.date    "expires_on"
  end

  create_table "subunits", :force => true do |t|
    t.integer "unit_id"
    t.text    "name"
  end

  add_index "subunits", ["unit_id"], :name => "unit_id"

  create_table "test_items", :force => true do |t|
    t.datetime "created_on",                        :null => false
    t.integer  "user_id",                           :null => false
    t.integer  "test_type_id"
    t.integer  "question_count", :default => 0
    t.integer  "correct_count",  :default => 0
    t.string   "status",         :default => "new"
  end

  add_index "test_items", ["test_type_id"], :name => "test_items_ibfk_2"
  add_index "test_items", ["user_id"], :name => "test_items_ibfk_1"

  create_table "test_questions", :force => true do |t|
    t.integer "test_item_id"
    t.integer "question_statement_id"
    t.integer "current_choice",        :default => -1
    t.text    "answer_sequence"
    t.integer "correct_choice"
  end

  add_index "test_questions", ["question_statement_id"], :name => "question_statement_id_test_questions"
  add_index "test_questions", ["test_item_id"], :name => "test_item_id"

  create_table "test_types", :force => true do |t|
    t.string "name", :limit => 60, :null => false
    t.string "code", :limit => 20, :null => false
  end

  create_table "underlying_concepts", :force => true do |t|
    t.integer "concept_statement_id"
    t.integer "question_statement_id"
    t.integer "is_multiple",           :default => 0
    t.integer "unit_id",                              :null => false
    t.integer "subunit_id",                           :null => false
    t.integer "exam_id"
  end

  add_index "underlying_concepts", ["concept_statement_id"], :name => "concept_statement_id"
  add_index "underlying_concepts", ["question_statement_id"], :name => "question_statement_id_underlying_concepts"
  add_index "underlying_concepts", ["subunit_id"], :name => "fk_underlying_concepts_subunits1"
  add_index "underlying_concepts", ["unit_id"], :name => "fk_underlying_concepts_units1"

  create_table "units", :force => true do |t|
    t.text    "name"
    t.boolean "show", :default => false
  end

  create_table "users", :force => true do |t|
    t.string   "login",           :limit => 80,                :null => false
    t.string   "salted_password", :limit => 40,                :null => false
    t.string   "email",           :limit => 60,                :null => false
    t.string   "firstname",       :limit => 40
    t.string   "lastname",        :limit => 40
    t.string   "salt",            :limit => 40,                :null => false
    t.integer  "verified",                      :default => 0
    t.string   "role",            :limit => 40
    t.string   "security_token",  :limit => 40
    t.datetime "token_expiry"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "logged_in_at"
    t.integer  "deleted",                       :default => 0
    t.datetime "delete_after"
  end

end
