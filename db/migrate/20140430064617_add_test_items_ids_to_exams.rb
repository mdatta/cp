class AddTestItemsIdsToExams < ActiveRecord::Migration
  def self.up
    add_column :exams, :test_type_ids, :text
  end

  def self.down
  end
end
