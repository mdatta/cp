class MoveSubPartAwayFromUSer < ActiveRecord::Migration
  def self.up
    remove_column :users , :subscription_level
    remove_column :users , :units_attempted
  end

  def self.down
  end
end
