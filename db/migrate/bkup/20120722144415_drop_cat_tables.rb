class DropCatTables < ActiveRecord::Migration
  def self.up
    drop_table  :exams_has_categories
    drop_table  :subcategories
  end

  def self.down
  end
end
