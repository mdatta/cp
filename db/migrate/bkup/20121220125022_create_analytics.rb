class CreateAnalytics < ActiveRecord::Migration
  def self.up
    create_table :analytics do |t|

      t.timestamps
    end
  end

  def self.down
    drop_table :analytics
  end
end
