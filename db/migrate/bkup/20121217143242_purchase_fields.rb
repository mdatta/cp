class PurchaseFields < ActiveRecord::Migration
  def self.up
  end
  def self.down
  end
  def change
    add_column :table => :purchases,:column => 'user_id',:type => :integer
    add_column :table => :purchases,:column => 'sub_id',:type => :integer
    add_column :table => :purchases,:column => 'amount',:type => :float
  end
end
