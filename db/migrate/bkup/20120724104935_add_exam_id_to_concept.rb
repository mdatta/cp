class AddExamIdToConcept < ActiveRecord::Migration
  def self.up
    add_column :underlying_concepts, :exam_id, :integer
    create_table :subscriptions do |s|
      s.column :user_id, :integer , :null => false
      s.column :exam_id, :integer , :null => false
    end
  end

  def self.down
  end
end
