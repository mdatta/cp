class AddSavedToTestItem < ActiveRecord::Migration
  def self.up
    add_column :test_items, :status, :string, :default => 'new'
  end
  def self.down
    remove_column :test_items, :status, :string
  end
end
