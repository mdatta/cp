require 'test_helper'

class PurchasesControllerTest < ActionController::TestCase
  test "should get pay_per_month" do
    get :pay_per_month
    assert_response :success
  end

  test "should get pay_per_month_subscribe" do
    get :pay_per_month_subscribe
    assert_response :success
  end

  test "should get pay_half_yearly" do
    get :pay_half_yearly
    assert_response :success
  end

  test "should get pay_yearly" do
    get :pay_yearly
    assert_response :success
  end

end
